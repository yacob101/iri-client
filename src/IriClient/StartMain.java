package IriClient;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;


//This is the main class where the program starts. It opens a new javaFX scene which is controlled by the MainSceneController.
public class StartMain extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource("MainScene.fxml"));
        primaryStage.setTitle("Iri-Client");
        primaryStage.setScene(new Scene(root, 1265, 700));
        primaryStage.show();
    }
}
