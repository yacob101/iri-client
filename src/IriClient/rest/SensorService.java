package IriClient.rest;

import IriClient.rest.model.Sensors;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SensorService {

     String BASE_URL = "http://localhost:8000";

    @GET("api/v1/sensors")
    Call<Sensors> getSensors();

    @GET("api/v1/sensors")
    Call<Sensors> getSensors(@Query("sensors") String sensorList);
}
