
package IriClient.rest.model;

import com.squareup.moshi.Json;

public class BinarySensor {

    @Json(name = "name")
    private String name1;
    @Json(name = "sensorValue")
    private Boolean sensorValue1;
    @Json(name = "timeStamp")
    private Double timeStamp1;
    @Json(name = "alarm")
    private String alarm1;

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public void setSensorValue1(Boolean sensorValue1) {
        this.sensorValue1 = sensorValue1;
    }

    public Double getTimeStamp1() {
        return timeStamp1;
    }

    public void setTimeStamp1(Double timeStamp1) {
        this.timeStamp1 = timeStamp1;
    }

    public String getAlarm1() {
        return alarm1;
    }

    public void setAlarm1(String alarm1) {
        this.alarm1 = alarm1;
    }

    public Boolean getSensorValue1() {
        return sensorValue1;
    }






}
