
package IriClient.rest.model;

import java.util.List;

import com.squareup.moshi.Json;

public class Sensor {

    @Json(name = "binarySensors")
    private List<BinarySensor> binarySensors = null;
    @Json(name = "analogSensors")
    private List<AnalogSensor> analogSensors = null;

    public List<BinarySensor> getBinarySensors() {
        return binarySensors;
    }

    public void setBinarySensors(List<BinarySensor> binarySensors) {
        this.binarySensors = binarySensors;
    }

    public List<AnalogSensor> getAnalogSensors() {
        return analogSensors;
    }

    public void setAnalogSensors(List<AnalogSensor> analogSensors) {
        this.analogSensors = analogSensors;
    }
}
