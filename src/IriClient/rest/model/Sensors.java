
package IriClient.rest.model;

import java.util.List;
import com.squareup.moshi.Json;

public class Sensors {

    @Json(name = "sensors")
    private List<Sensor> sensors = null;

    public List<Sensor> getSensors() {
        return sensors;
    }

    public void setSensors(List<Sensor> sensors) {
        this.sensors = sensors;
    }
}
