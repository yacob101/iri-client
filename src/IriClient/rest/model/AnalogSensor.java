package IriClient.rest.model;

import com.squareup.moshi.Json;

public class AnalogSensor {

    @Json(name = "name")
    private String name;
    @Json(name = "sensorValue")
    private Integer sensorValue;
    @Json(name = "unit")
    private String unit;
    @Json(name = "timeStamp")
    private double timeStamp ;
    @Json(name = "dateTimeMs")
    private String dateTimeMs;
    @Json(name = "lowerBound")
    private Integer lowerBound;
    @Json(name = "upperBound")
    private Integer upperBound;
    @Json(name = "lowerBoundAlarm")
    private Boolean lowerBoundAlarm;
    @Json(name = "upperBoundAlarm")
    private Boolean upperBoundAlarm;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSensorValue() {
        return sensorValue;
    }

    public void setSensorValue(Integer sensorValue) {
        this.sensorValue = sensorValue;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Double getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Double timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Integer getLowerBound() {
        return lowerBound;
    }

    public void setLowerBound(Integer lowerBound) {
        this.lowerBound = lowerBound;
    }

    public Integer getUpperBound() {
        return upperBound;
    }

    public void setUpperBound(Integer upperBound) {
        this.upperBound = upperBound;
    }

    public Boolean getLowerBoundAlarm() {
        return lowerBoundAlarm;
    }

    public void setLowerBoundAlarm(Boolean lowerBoundAlarm) {
        this.lowerBoundAlarm = lowerBoundAlarm;
    }

    public Boolean getUpperBoundAlarm() {
        return upperBoundAlarm;
    }

    public void setUpperBoundAlarm(Boolean upperBoundAlarm) {
        this.upperBoundAlarm = upperBoundAlarm;
    }



}
