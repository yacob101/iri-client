package IriClient;

import IriClient.rest.model.AnalogSensor;
import IriClient.rest.model.BinarySensor;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
//contains return objects for two different fucntions
public class ReturnObject {

    public  ObservableList<AnalogSensor> analogSensorObservableList = FXCollections.observableArrayList();
    public  ObservableList<BinarySensor> binarySensorObservableList = FXCollections.observableArrayList();
}

  class AlarmArray {

    public ArrayList<String> alarmList = new ArrayList<>();
    public ArrayList<Long> triggerTime = new ArrayList<>();
}
