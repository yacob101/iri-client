package IriClient;

import IriClient.rest.SensorService;
import IriClient.rest.model.AnalogSensor;
import IriClient.rest.model.BinarySensor;
import IriClient.rest.model.Sensor;
import IriClient.rest.model.Sensors;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;


public class MainSceneController extends AnalogSensor implements Initializable, Callback<Sensors> {

    private static List<Integer> allValuesLast12Hour = new ArrayList<>();
    @FXML
    public ScatterChart<?, Integer> scatterChart;
    private List<String> printList = new ArrayList<>();
    private AlarmArray alarmArray = new AlarmArray();
    private ReturnObject returnObject = new ReturnObject();
    private List<Integer> analogValueList = new ArrayList<>();
    private List<Long> allValuesLast12HourTime = new ArrayList<>();
    private List<List<Integer>> firstGraphList = new ArrayList<>();
    private List<List<Integer>> secondGraphList = new ArrayList<>();
    //Default X values for the graph
    private List<String> timeChart2h = new ArrayList<>(Arrays.asList("T-2:00", "T-1:50", "T-1:40", "T-1:30", "T-1:20", "T-1:10", "T-1:00", "T-0:50", "T-0:40", "T-0:30", "T-0:20", "T-0:10", "T-0:00"));
    //Default X values for the graph
    private List<String> timeChart12h = new ArrayList<>(Arrays.asList("T-12:00", "T-11:00", "T-10:00", "T-9:00", "T-8:00", "T-7:00", "T-6:00", "T-5:00", "T-4:00", "T-3:00", "T-2:00", "T-1:00", "T-0:00"));
    private String returnSensorNumberString;
    @FXML
    private Button showPastAlarms;
    @FXML
    private TableView<AnalogSensor> analogTable;
    @FXML
    private TableColumn<AnalogSensor, String> name;
    @FXML
    private TableColumn<AnalogSensor, Integer> sensorValue;
    @FXML
    private TableColumn<AnalogSensor, String> unit;
    @FXML
    private TableColumn<AnalogSensor, Double> timeStamp;
    @FXML
    private TableColumn<AnalogSensor, Integer> lowerBound;
    @FXML
    private TableColumn<AnalogSensor, Integer> upperBound;
    @FXML
    private TableColumn<AnalogSensor, Boolean> lowerBoundAlarm;
    @FXML
    private TableColumn<AnalogSensor, Boolean> upperBoundAlarm;
    @FXML
    private TableView<BinarySensor> binaryTable;
    @FXML
    private TableColumn<BinarySensor, String> name1;
    @FXML
    private TableColumn<BinarySensor, Boolean> sensorValue1;
    @FXML
    private TableColumn<BinarySensor, Double> timeStamp1;
    @FXML
    private TableColumn<BinarySensor, String> alarm1;
    @FXML
    private ScatterChart<?, Integer> scatterChart1;
    @FXML

    private XYChart.Series series = new XYChart.Series();
    @FXML
    private XYChart.Series series1 = new XYChart.Series();


    //-------------------------------------------------------------------------------------------------------------------
    //This is the "main function" for the scene controller after the scene loads
    public void initialize(URL url, ResourceBundle rb) {
//loads data from the Iri server
        returnObject = getReturnObject();
        initializeTablesAndGraphs();
        setItems();
        miniSleep();
        readIriDatabaseLoop();
        firstGraphUpdate();
        secondGraphUpdate();
        miniSleep();
        firstGraphLoop10m();
        secondGraphLoop1h();
    }

    //--------------------------------------------------------------------------------------------------------------------

    //Initializes the Tableview and ScatterCharts
    private void initializeTablesAndGraphs() {
        //Initialize analog table
        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        sensorValue.setCellValueFactory(new PropertyValueFactory<>("sensorValue"));
        unit.setCellValueFactory(new PropertyValueFactory<>("unit"));
        timeStamp.setCellValueFactory(new PropertyValueFactory<>("timeStamp"));
        lowerBound.setCellValueFactory(new PropertyValueFactory<>("lowerBound"));
        upperBound.setCellValueFactory(new PropertyValueFactory<>("upperBound"));
        lowerBoundAlarm.setCellValueFactory(new PropertyValueFactory<>("lowerBoundAlarm"));
        upperBoundAlarm.setCellValueFactory(new PropertyValueFactory<>("upperBoundAlarm"));
        //Initialize binary table
        name1.setCellValueFactory(new PropertyValueFactory<>("name1"));
        sensorValue1.setCellValueFactory(new PropertyValueFactory<>("sensorValue1"));
        timeStamp1.setCellValueFactory(new PropertyValueFactory<>("timeStamp1"));
        alarm1.setCellValueFactory(new PropertyValueFactory<>("alarm1"));
        //enables the method to change sensor values by clicking on a certain item in the chart.
        analogTable.setOnMouseClicked(this::analogTableClickedMethod);

        initializeGraphs();
    }

    //initializes graphs and enables animation over time
    private void initializeGraphs() {
        scatterChart.getData().add(series);
        scatterChart1.getData().add(series1);
        scatterChart.setAnimated(true);
        scatterChart1.setAnimated(true);
    }

    //This method allows you to change the upper and lower bounds of sensors by clicking on them in the analog table.
    private void analogTableClickedMethod() {
        try {
            String selectedSensorName = analogTable.getSelectionModel().getSelectedItem().getName();
            String selectedSensorLowerBound = analogTable.getSelectionModel().getSelectedItem().getLowerBound().toString();
            String selectedSensorUpperBound = analogTable.getSelectionModel().getSelectedItem().getUpperBound().toString();
            returnSensorNumberString = selectedSensorName.replaceAll("[^\\d.]", "");
            System.out.println(returnSensorNumberString);
            final Stage dialog = new Stage();
            dialog.setTitle("Change " + selectedSensorName);
            dialog.initModality(Modality.APPLICATION_MODAL);
            Button button = new Button("Update Alarm Boundaries");
            TextField lowerBoundTextField = new TextField(selectedSensorLowerBound);
            TextField upperBoundTextField = new TextField(selectedSensorUpperBound);
            VBox upperBoundBox = new VBox(10);
            upperBoundBox.getChildren().add(new Text("Change " + selectedSensorName + " lowerbound"));
            upperBoundBox.getChildren().add(lowerBoundTextField);
            upperBoundBox.getChildren().add(new Text("Change " + selectedSensorName + " upperbound"));
            upperBoundBox.getChildren().add(upperBoundTextField);
            upperBoundBox.getChildren().add(button);
            returnSensorNumberString = removeLeadingZeroes(returnSensorNumberString);
            int result = Integer.parseInt(returnSensorNumberString);
            returnSensorNumberString = Integer.toString(result - 1);

            button.setOnMouseClicked(event -> {
                String returnString = (returnSensorNumberString + "," + lowerBoundTextField.getText() + "," + upperBoundTextField.getText());
                //Checks if the data from the fields entered only contains numerical values, so no strings or invalid input can be entered.
                if (returnString.matches("[0-9,]+") && returnString.length() > 2) {
                    System.out.println("De return string is " + returnString);
                    postNewSensorBoundaryValues(returnString);
                } else {
                    //If any invalid input is entered, a dialogbox pops up with an error message.
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Invalid Boundary");
                    alert.setHeaderText("Invalid input:");
                    alert.setContentText("Non numerical value entered");
                    alert.showAndWait();
                }
            });
            Scene dialogScene = new Scene(upperBoundBox, 400, 200);
            dialog.setScene(dialogScene);
            dialog.show();
        } catch (Exception nothingSelected) {
            System.out.println("Nothing Selected");
        }
    }
//Posts the new boundary value jfor the sensor to the server
    private void postNewSensorBoundaryValues(String boundaryString) {
        postSensors(boundaryString);
    }
//used to get the index of the sensor clicked
    private String removeLeadingZeroes(String value) {
        return new Integer(value).toString();
    }
    //Wait for response from server before trying to fill the fields.
    private void miniSleep() {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //Sets all objects in the tables
    public void setItems() {
        returnObject = getReturnObject();

        binaryTable.setItems(returnObject.binarySensorObservableList);
        analogTable.setItems(returnObject.analogSensorObservableList);
        analogValueList = getAnalogValueList();
        sensorAlarmCheckSound();


    }

    //This loop reads the Iri server every seconds and retrieves new values.
    private void readIriDatabaseLoop() {
        Timeline firstGraphLoop = new Timeline();
        firstGraphLoop.getKeyFrames().add(
                new KeyFrame(Duration.millis(1000), (ActionEvent actionEvent) -> {
                    setItems();
                    sensorAlarmCheckSound();
                }));
        firstGraphLoop.setCycleCount(-1);
        firstGraphLoop.setAutoReverse(true);  //!?
        firstGraphLoop.play();
    }


    //This loop gets the value for the first graph every 10 minutes by reading the current values in the analogTable.
    //NOTE: KeyFrame(Duration.seconds(1) needs to be changed to KeyFrame(Duration.minutes(10) for the real database. This is just a demo
    private void firstGraphLoop10m() {
        Timeline firstGraphLoop = new Timeline();
        firstGraphLoop.getKeyFrames().add(
                new KeyFrame(Duration.seconds(1), (ActionEvent actionEvent) -> {
//Reads most recent analogSensor values from the analog table and inserts them into the graph
                    firstGraphUpdate();
//
                }));
        firstGraphLoop.setCycleCount(-1);
        firstGraphLoop.setAutoReverse(true);  //!?
        firstGraphLoop.play();
    }

    //Same as the first graph, except the timings are different.
    //NOTE: KeyFrame(Duration.seconds(5) needs to be changed to KeyFrame(Duration.hours(1) for the real database. This is just a demo
    private void secondGraphLoop1h() {
        Timeline secondGraphLoop = new Timeline();
        secondGraphLoop.getKeyFrames().add(
                new KeyFrame(Duration.seconds(5), (ActionEvent actionEvent) -> {

                    secondGraphUpdate();
//                    printAlarms();
                }));
        secondGraphLoop.setCycleCount(-1);
        secondGraphLoop.setAutoReverse(true);  //!?
        secondGraphLoop.play();
    }

    //This is the first graph update method. It's in a seperate method because it needs to load all the graph values before starting the loop.
    private void firstGraphUpdate() {
        series.getData().clear();
        while (firstGraphList.size() < 13) {

            firstGraphList.add(new ArrayList<>(Arrays.asList(0)));
        }
        for (int i = 0; i < 12; i++) {
            firstGraphList.get(i).clear();
            firstGraphList.get(i).addAll(firstGraphList.get(i + 1));
        }
        firstGraphList.remove(12);
        firstGraphList.add(analogValueList);

        for (int i = 0; i < 13; i++) {
            for (Integer integer : firstGraphList.get(i)) {
                series.getData().add(new XYChart.Data(timeChart2h.get(i), integer));
            }
        }

        try {
            //This is to add all values to the 12 hour list for printing
            for (Integer intje : analogValueList) {
                allValuesLast12Hour.add(intje);
                allValuesLast12HourTime.add(Instant.now().toEpochMilli());
            }
//        System.out.println(alarmArray.alarmList);
//        System.out.println(alarmArray.triggerTime);
            for (int i = 0; allValuesLast12HourTime.get(0) + 3001 <= Instant.now().toEpochMilli(); i++) {
                allValuesLast12Hour.remove(0);
                allValuesLast12HourTime.remove(0);
                System.out.println("Removed:" + allValuesLast12Hour.get(0));
                System.out.println("time:" + allValuesLast12HourTime.get(0));
            }
        } catch (Exception e) {
            System.out.println("Graph udate method: no values in last12h time");
        }
    }

    //updates second graph
    private void secondGraphUpdate() {
        analogValueList = getAnalogValueList();
        System.out.println(analogValueList);

        series1.getData().clear();
        while (secondGraphList.size() < 13) {
            System.out.println(secondGraphList.size());
            secondGraphList.add(new ArrayList<>(Arrays.asList(0)));
            System.out.println(secondGraphList);
        }
        for (int i = 0; i < 12; i++) {
            secondGraphList.get(i).clear();
            secondGraphList.get(i).addAll(secondGraphList.get(i + 1));
        }
        secondGraphList.remove(12);
        secondGraphList.add(analogValueList);

        for (int i = 0; i < 13; i++) {
            for (Integer integer : secondGraphList.get(i)) {
                series1.getData().add(new XYChart.Data(timeChart12h.get(i), integer));
            }
        }
    }

    //Calculates the avarage of a lost of integers for the printing list
    private double calculateAverage(List<Integer> marks) {
        Integer sum = 0;
        if (!marks.isEmpty()) {
            for (Integer mark : marks) {
                sum += mark;
            }
            return sum.doubleValue() / marks.size();
        }
        return sum;
    }

    //Checks if any sensors have a triggered alarm. If any alarm is true, add that alarm to the list.
    public void sensorAlarmCheckSound() {
        try {
            for (AnalogSensor item : analogTable.getItems()) {
                if (lowerBoundAlarm.getCellObservableValue(item).getValue()) {
               playSound();
                    alarmArrayAdd("Lowerbound Alarm " + item.getName());
                    //temp comment                   System.out.println("Alarm:" + name.getCellObservableValue(item).getValue());
                }
                if (upperBoundAlarm.getCellObservableValue(item).getValue()) {
                playSound();
                    alarmArrayAdd("Uppberbound Alarm " + item.getName());
                }
            }
            for (BinarySensor item : binaryTable.getItems()) {
                if (!alarm1.getCellObservableValue(item).getValue().equals("no alarm")) {
                playSound();

                    alarmArrayAdd(name1.getCellObservableValue(item).getValue());
                }
            }
        } catch (Exception e) {
            System.out.println("SensorAlarmSoundError");
        }
    }

    //Gets all sensorvalues from the analogtable and returns it a a list
    private List<Integer> getAnalogValueList() {
        List<Integer> listHolder = new ArrayList<>();
        try {
            for (AnalogSensor item : analogTable.getItems()) {
                listHolder.add(sensorValue.getCellObservableValue(item).getValue());
            }
        } catch (Exception e) {
            System.out.println("analog nog filled");
        }
        return listHolder;
    }

    //returns two observable arraylists.
    private ReturnObject getReturnObject() {
        loadSensors();
        return returnObject;
    }

    //loads the JSON from the base url, which is defined in the Sensors class.
    private void loadSensors() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SensorService.BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build();

        SensorService sensorService = retrofit.create(SensorService.class);
        sensorService.getSensors().enqueue(this);
    }

    //sends data to the C program. This is used to change the alarm boundaries in the analogTableClicked method
    private void postSensors(String sensors) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SensorService.BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build();

        SensorService sensorService = retrofit.create(SensorService.class);
        sensorService.getSensors(sensors).enqueue(this);
    }

    //When it reads data from the server, in this case the response from the body, add all those sensors to an observable list.
    public void onResponse(Call<Sensors> call, Response<Sensors> response) {
        try {
            //Cleart obvservableList zodat de refresh ge add kan worden
            returnObject.analogSensorObservableList.clear();
            returnObject.binarySensorObservableList.clear();
            //Pakt de lijst van alle sensors en stopt deze  in allSensors
            Sensor allSensors = response.body().getSensors().get(0);
            //Gaat door alle analogSensors heen en vult de  returnObject.analogSensorObservableList lijst met analogSensors
            returnObject.analogSensorObservableList.addAll(allSensors.getAnalogSensors());
            returnObject.binarySensorObservableList.addAll(allSensors.getBinarySensors());
        } catch (Exception e) {
            System.out.println("filling tables error");
        }

    }

    //error message when call doesn't work
    @Override
    public void onFailure(Call<Sensors> call, Throwable thrwbl) {
        System.out.println(thrwbl.getMessage());
    }

    //activates method when the button is clicked
    private void analogTableClickedMethod(MouseEvent event) {
        analogTableClickedMethod();
    }

    //adds alarm to alarm array in last 12 hours, removes alarms older than 12 hours.
    public void alarmArrayAdd(String alarmName) {
        alarmArray.alarmList.add(alarmName);
        alarmArray.triggerTime.add(Instant.now().toEpochMilli());
//        System.out.println(alarmArray.alarmList);
//        System.out.println(alarmArray.triggerTime);
        removeOldAlarms();
    }

    //removes old alarms by checking whether their timestamp is 12 hours older than the current time
    private void removeOldAlarms() {

        while (alarmArray.triggerTime.get(0) + 43200000 < Instant.now().toEpochMilli()) {
            alarmArray.triggerTime.remove(0);
            alarmArray.alarmList.remove(0);
        }
    }


    //shows alarms presssed in the last 12 hours
    public void onShowAlarmButtonPressed() {
        removeOldAlarms();
        Dialog<AlarmArray> dialog = new Dialog<>();
        dialog.setTitle("Alarms triggered last 12 hours");
        ListView<String> alarmListView = new ListView();
        List<String> valueHolder = alarmArray.alarmList;
        ObservableList<String> items = FXCollections.observableArrayList(valueHolder);
        alarmListView.setItems(items);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
        Node closeButton = dialog.getDialogPane().lookupButton(ButtonType.CLOSE);
        closeButton.managedProperty().bind(closeButton.visibleProperty());
        closeButton.setVisible(true);
        GridPane grid = new GridPane();
        grid.add(alarmListView, 1, 1);
        grid.add(closeButton, 1, 1);
        dialog.getDialogPane().setContent(grid);
        dialog.showAndWait();
        //This prints the alarns as a demo. In the real code this function would be in the 1hour loop.
        printAlarms();
    }

    //Takes the minimum, maximum, and avarage of all values in the last 12 hours, which were put in that list every 10 minutes, and makes a printable object.
    //At the end of this method the object made is printed and then closed.
    public void printAlarms() {

        printList.clear();

        int min = allValuesLast12Hour.get(0);
        int max = allValuesLast12Hour.get(0);
        //iterate through the list and find the max and min values
        for (int i = 0; i < (allValuesLast12HourTime.size() - 1); i++) {
            if (allValuesLast12Hour.get(i) > max) {
                max = allValuesLast12Hour.get(i);
            }
            if (allValuesLast12Hour.get(i) < min) {
                min = allValuesLast12Hour.get(i);
            }
        }
        printList.add("Minumum value last 12h: " + Integer.toString(min));
        printList.add("Maximum value last 12h: " + Integer.toString(max));
        printList.add("Avarage value last 12h: " + Double.toString(calculateAverage(allValuesLast12Hour)));
        System.out.println("min = " + min + "max is " + max);
        System.out.println("PRINTLIST IS" + printList);
        Dialog<AlarmArray> dialog = new Dialog<>();

        dialog.setTitle("Alarms triggered last 12 hours");
        ObservableList<String> observablePrintList = FXCollections.observableArrayList(printList);
        ListView<String> alarmListView = new ListView();
        alarmListView.setItems(observablePrintList);
        Pane pane = new Pane();
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
        Node closeButton = dialog.getDialogPane().lookupButton(ButtonType.CLOSE);
        closeButton.managedProperty().bind(closeButton.visibleProperty());
        closeButton.setVisible(true);
        pane.getChildren().addAll(alarmListView);
        dialog.getDialogPane().setContent(pane);
        dialog.show();
        print(pane);
        dialog.close();
    }

    //Prints a node, in this case the pane.
    public void print(Node node) {
        Printer printer = Printer.getDefaultPrinter();
        PrinterJob job = PrinterJob.createPrinterJob();
        if (job != null) {
            boolean success = job.printPage(node);
            if (success) {
                job.endJob();
            }
        }
    }


}